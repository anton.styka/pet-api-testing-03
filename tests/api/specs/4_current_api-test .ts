import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("anton66.styka@gmail.com", "Ant0n");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`should return correct details of the current user`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        
    });

        
});
