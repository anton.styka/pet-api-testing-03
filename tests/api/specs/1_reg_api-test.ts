import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { RegController } from "../lib/controllers/reg.controller";

const users = new UsersController();
const reg = new RegController();

describe("Token usage", () => {
    let accessToken: string;

    before(`Register and get the token`, async () => {
        let response = await reg.regUser(0, "string", "anton66.styka@gmail.com", "anton", "Ant0n");

        accessToken = response.body.token.accessToken.token;
        // console.reg(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 6962,
            avatar: "string",
            email: "anton66.styka@gmail.com",
            userName: "anton",
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
    });
});
