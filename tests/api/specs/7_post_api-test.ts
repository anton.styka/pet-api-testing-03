import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { PostController } from "../lib/controllers/post.controller";
import { AuthController } from "../lib/controllers/auth.controller";


const post = new PostController();
const auth = new AuthController();


xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("anton66.styka@gmail.com", "Ant0n");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`Add post`, async () => {
        let postData: object = {
            authorId: 0,
            image: "string",
            body: "string",
        };

        let response = await post.addPost(postData, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });

    it(`Add comment`, async () => {
        let postComment: object = {
            authorId: 0,
            postId: 3722,
            body: "string",
        };

        let response = await post.addComment(postComment, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });

    it(`Add like`, async () => {
        let postLike: object = {
            userId: 0,
            isLike: true,
            entityId: 3722,
        };

        let response = await post.addLike(postLike, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await post.getAllPosts();

        console.log("All Posts:");
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        
        
    });

});
