import { ApiRequest } from "../request";


const baseUrl: string = global.appConfig.baseUrl;


export class PostController {
    async addPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addComment(postComment: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(postComment)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLike(postLike: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(postLike)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

}
