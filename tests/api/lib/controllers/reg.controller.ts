import { ApiRequest } from "../request";

export class RegController {
    async regUser(id: 0, avatarValue: string, emailValue: string, usernameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                id: 0,
                avatar: avatarValue,
                email: emailValue,
                username: usernameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
